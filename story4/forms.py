from django import forms
from django.forms import ModelForm
from .models import ClassYear, Friends
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

class InputForm(ModelForm):
    class Meta:
        model = Friends
        fields = '__all__'
