from django.db import models
from django.utils import timezone

# Create your models here.

class ClassYear(models.Model):
    year = models.IntegerField(default=2019)
    def __str__(self):
        return "%s" %(self.year)

class Friends(models.Model):
    name = models.CharField(max_length=30)
    hobby = models.CharField(max_length=50)
    favorite_food = models.CharField(max_length=30)
    year = models.ForeignKey(ClassYear, on_delete=models.CASCADE, default=2019)
    def __str__(self):
        return self.name

