from django.shortcuts import render, redirect
from django .http import HttpResponse
from .forms import InputForm
from .models import Friends

def index(request):
    return render(request, "index.html")

def about(request):
    return render(request, "about.html")

def resume(request):
    return render(request, "resume.html")

def contact(request):
    return render(request, "contact.html")


def forms(request):
    if request.method == 'POST':
        form = InputForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'form.html')
    else:
        form = InputForm()
        return render(request, 'form.html', {'form':form})

def friend(request):
    allfriends = Friends.objects.all()
    context = {'allfriends':allfriends}
    return render(request, 'friend.html', context)
    